/**
 * 李震
 * 我的码云：https://git.oschina.net/git-lizhen
 * 我的CSDN：http://blog.csdn.net/weixin_38215395
 * 联系：lizhen@sia.cn
 */
#ifndef MATCH_H
#define MATCH_H


class Match
{
public:
    Match(int ZhuChangJinQiuShu,int KechangJinQiuShu);   //构造函数
    int ZhuChangJiFen();         //返回该场比赛主场获得积分
    int KeChangJiFen();          //返回该场比赛客场获得积分
    int ZhuChangJingShengQiu();  //返回该场比赛主场净胜球数
    int KeChangJingShengQiu();   //返回该场比赛客场净胜球数
    ~Match();                    //析构函数

private:
    int ZhuChang_JiFen;           //主场比赛得分
    int ZhuChang_JingShengQiu;    //主场净胜球数
    int KeChang_JiFen;            //客场比赛得分
    int KeChang_JingShengQiu;     //客场净胜球数
};

#endif // MATCH_H
