/**
 * 李震
 * 我的码云：https://git.oschina.net/git-lizhen
 * 我的CSDN：http://blog.csdn.net/weixin_38215395
 * 联系：lizhen@sia.cn
 */
#include "Match.h"

//构造函数
Match::Match(int ZhuChangJinQiuShu,int KechangJinQiuShu)
{
    if(ZhuChangJinQiuShu==KechangJinQiuShu)     //比赛平局
    {
        ZhuChang_JiFen=1;
        KeChang_JiFen=1;
    }
    else if(ZhuChangJinQiuShu>KechangJinQiuShu) //主场胜
    {
        ZhuChang_JiFen=3;
        KeChang_JiFen=-3;
    }
    else                                        //主场负
    {
        ZhuChang_JiFen=-3;
        KeChang_JiFen=3;
    }
    ZhuChang_JingShengQiu=ZhuChangJinQiuShu-KechangJinQiuShu;
    KeChang_JingShengQiu=KechangJinQiuShu-ZhuChangJinQiuShu;
}
//析构函数
Match::~Match()
{

}
int Match::ZhuChangJiFen()
{
    return ZhuChang_JiFen;
}
//public函数,返回该场比赛主场净胜球数
int Match::ZhuChangJingShengQiu()
{
    return ZhuChang_JingShengQiu;
}
//public函数,返回该场比赛客场获得积分
int Match::KeChangJiFen()
{
    return KeChang_JiFen;
}
//public函数,返回该场比赛客场净胜球数
int Match::KeChangJingShengQiu()
{
    return KeChang_JingShengQiu;
}










