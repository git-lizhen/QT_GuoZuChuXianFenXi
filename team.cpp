/**
 * 李震
 * 我的码云：https://git.oschina.net/git-lizhen
 * 我的CSDN：http://blog.csdn.net/weixin_38215395
 * 联系：lizhen@sia.cn
 */
#include "team.h"

//构造函数
//参数依次为：当前积分，当前净胜球数，是否为主场（bool值),比赛结果主场得分，比赛结果客场得分
Team::Team(int JiFen_Present,int JingShengQiu_Present,bool ZhuChang,int ZhuChangJinQiuShu,int KeChangJinQiuShu)
{
    int JiFenAfter_Zhu;                     //赛后主场积分
    int JiFenAfter_Ke;                      //赛后客场积分
    if(ZhuChangJinQiuShu>KeChangJinQiuShu)  //主场赢球
    {
        JiFenAfter_Zhu=JiFen_Present+3;
        JiFenAfter_Ke=JiFen_Present;
    }
    else if(ZhuChangJinQiuShu<KeChangJinQiuShu)//客场赢球
    {
        JiFenAfter_Zhu=JiFen_Present;
        JiFenAfter_Ke=JiFen_Present+3;
    }
    else                                    //平局
    {
        JiFenAfter_Zhu=JiFen_Present+1;
        JiFenAfter_Ke=JiFen_Present+1;
    }
    //计算比赛后的积分和净胜球数
    if(ZhuChang==1)    //主场作战
    {
        JiFenAfter=JiFenAfter_Zhu;
        JingShengQiuAfter=JingShengQiu_Present+ZhuChangJinQiuShu-KeChangJinQiuShu;
    }
    else               //客场作战
    {
        JiFenAfter=JiFenAfter_Ke;
        JingShengQiuAfter=JingShengQiu_Present+KeChangJinQiuShu-ZhuChangJinQiuShu;
    }
}
//析构函数
Team::~Team()
{
}
//public函数,返回比赛后球队积分
int Team::JiFen_After()
{
    return JiFenAfter;
}
//public函数,返回比赛后球队净胜球数
int Team::JingShengQiu_After()
{
    return JingShengQiuAfter;
}
