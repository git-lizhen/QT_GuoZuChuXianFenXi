/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QLabel *label;
    QWidget *gridWidget;
    QGridLayout *gridLayout;
    QLabel *label_3;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_5;
    QLabel *label_8;
    QLabel *label_4;
    QLabel *label_2;
    QWidget *gridWidget_2;
    QGridLayout *gridLayout_2;
    QLabel *label_17;
    QLabel *label_13;
    QLabel *label_18;
    QLabel *label_16;
    QLabel *label_14;
    QLabel *label_15;
    QLabel *label_19;
    QWidget *gridWidget_3;
    QGridLayout *gridLayout_3;
    QLabel *label_20;
    QLabel *label_21;
    QLabel *label_22;
    QLabel *label_23;
    QLabel *label_24;
    QLabel *label_25;
    QLabel *label_26;
    QWidget *gridWidget_4;
    QGridLayout *gridLayout_4;
    QLabel *label_30;
    QLabel *label_28;
    QLabel *label_31;
    QLabel *label_32;
    QWidget *gridWidget_5;
    QGridLayout *gridLayout_5;
    QLineEdit *Iran_Score_Edit;
    QLineEdit *Katar_Score_Edit;
    QLineEdit *China_Score_Edit;
    QLineEdit *Uzbekistan_Score_Edit;
    QLineEdit *Syria_Score_Edit;
    QLineEdit *Korea_Score_Edit;
    QLabel *label_27;
    QLabel *label_29;
    QLabel *label_33;
    QLabel *label_34;
    QTextBrowser *Nine_Analyze_text;
    QLabel *label_10;
    QPushButton *After_nine_Button;
    QLabel *label_11;
    QWidget *gridWidget_7;
    QGridLayout *gridLayout_7;
    QLabel *Syria_JingShengQiu_label;
    QLabel *label_40;
    QLabel *Katar_JingShengQiu_label;
    QLabel *Uzbekistan_JingShengQiu_label;
    QLabel *Korea_JingShengQiu_label;
    QLabel *Iran_JingShengQiu_label;
    QLabel *China_JingShengQiu_label;
    QWidget *gridWidget_8;
    QGridLayout *gridLayout_8;
    QLabel *Syria_JiFen_label;
    QLabel *Katar_JiFen_label;
    QLabel *Uzbekistan_JiFen_label;
    QLabel *Korea_JiFen_label;
    QLabel *Iran_JiFen_label;
    QLabel *China_JiFen_label;
    QLabel *label_47;
    QWidget *gridWidget_9;
    QGridLayout *gridLayout_9;
    QLabel *label_9;
    QLabel *label_12;
    QLabel *label_53;
    QLabel *label_54;
    QLabel *label_55;
    QLabel *label_56;
    QLabel *label_57;
    QLabel *label_35;
    QPushButton *Now_Button;
    QGraphicsView *graphicsView;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QStringLiteral("Widget"));
        Widget->resize(972, 602);
        Widget->setLayoutDirection(Qt::LeftToRight);
        label = new QLabel(Widget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(360, 30, 281, 21));
        QFont font;
        font.setFamily(QString::fromUtf8("\345\256\213\344\275\223"));
        font.setPointSize(16);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        gridWidget = new QWidget(Widget);
        gridWidget->setObjectName(QStringLiteral("gridWidget"));
        gridWidget->setGeometry(QRect(50, 70, 175, 201));
        gridLayout = new QGridLayout(gridWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_3 = new QLabel(gridWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("\345\256\213\344\275\223"));
        font1.setPointSize(12);
        label_3->setFont(font1);
        label_3->setLayoutDirection(Qt::LeftToRight);

        gridLayout->addWidget(label_3, 3, 1, 1, 1);

        label_6 = new QLabel(gridWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setFont(font1);

        gridLayout->addWidget(label_6, 6, 1, 1, 1);

        label_7 = new QLabel(gridWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setFont(font1);

        gridLayout->addWidget(label_7, 7, 1, 1, 1);

        label_5 = new QLabel(gridWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setFont(font1);

        gridLayout->addWidget(label_5, 5, 1, 1, 1);

        label_8 = new QLabel(gridWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setFont(font1);

        gridLayout->addWidget(label_8, 8, 1, 1, 1);

        label_4 = new QLabel(gridWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font1);

        gridLayout->addWidget(label_4, 4, 1, 1, 1);

        label_2 = new QLabel(gridWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("\345\256\213\344\275\223"));
        font2.setPointSize(14);
        font2.setBold(true);
        font2.setWeight(75);
        label_2->setFont(font2);

        gridLayout->addWidget(label_2, 1, 1, 1, 1);

        gridWidget_2 = new QWidget(Widget);
        gridWidget_2->setObjectName(QStringLiteral("gridWidget_2"));
        gridWidget_2->setGeometry(QRect(190, 70, 175, 201));
        gridLayout_2 = new QGridLayout(gridWidget_2);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        label_17 = new QLabel(gridWidget_2);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setFont(font1);

        gridLayout_2->addWidget(label_17, 6, 1, 1, 1);

        label_13 = new QLabel(gridWidget_2);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setFont(font2);

        gridLayout_2->addWidget(label_13, 1, 1, 1, 1);

        label_18 = new QLabel(gridWidget_2);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setFont(font1);

        gridLayout_2->addWidget(label_18, 7, 1, 1, 1);

        label_16 = new QLabel(gridWidget_2);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setFont(font1);

        gridLayout_2->addWidget(label_16, 5, 1, 1, 1);

        label_14 = new QLabel(gridWidget_2);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setFont(font1);

        gridLayout_2->addWidget(label_14, 4, 1, 1, 1);

        label_15 = new QLabel(gridWidget_2);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setFont(font1);
        label_15->setLayoutDirection(Qt::LeftToRight);

        gridLayout_2->addWidget(label_15, 3, 1, 1, 1);

        label_19 = new QLabel(gridWidget_2);
        label_19->setObjectName(QStringLiteral("label_19"));
        label_19->setFont(font1);

        gridLayout_2->addWidget(label_19, 8, 1, 1, 1);

        gridWidget_3 = new QWidget(Widget);
        gridWidget_3->setObjectName(QStringLiteral("gridWidget_3"));
        gridWidget_3->setGeometry(QRect(300, 70, 175, 201));
        gridLayout_3 = new QGridLayout(gridWidget_3);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        label_20 = new QLabel(gridWidget_3);
        label_20->setObjectName(QStringLiteral("label_20"));
        label_20->setFont(font1);

        gridLayout_3->addWidget(label_20, 6, 1, 1, 1);

        label_21 = new QLabel(gridWidget_3);
        label_21->setObjectName(QStringLiteral("label_21"));
        label_21->setFont(font2);

        gridLayout_3->addWidget(label_21, 1, 1, 1, 1);

        label_22 = new QLabel(gridWidget_3);
        label_22->setObjectName(QStringLiteral("label_22"));
        label_22->setFont(font1);

        gridLayout_3->addWidget(label_22, 7, 1, 1, 1);

        label_23 = new QLabel(gridWidget_3);
        label_23->setObjectName(QStringLiteral("label_23"));
        label_23->setFont(font1);

        gridLayout_3->addWidget(label_23, 5, 1, 1, 1);

        label_24 = new QLabel(gridWidget_3);
        label_24->setObjectName(QStringLiteral("label_24"));
        label_24->setFont(font1);

        gridLayout_3->addWidget(label_24, 4, 1, 1, 1);

        label_25 = new QLabel(gridWidget_3);
        label_25->setObjectName(QStringLiteral("label_25"));
        label_25->setFont(font1);
        label_25->setLayoutDirection(Qt::LeftToRight);

        gridLayout_3->addWidget(label_25, 3, 1, 1, 1);

        label_26 = new QLabel(gridWidget_3);
        label_26->setObjectName(QStringLiteral("label_26"));
        label_26->setFont(font1);

        gridLayout_3->addWidget(label_26, 8, 1, 1, 1);

        gridWidget_4 = new QWidget(Widget);
        gridWidget_4->setObjectName(QStringLiteral("gridWidget_4"));
        gridWidget_4->setGeometry(QRect(470, 60, 175, 201));
        gridLayout_4 = new QGridLayout(gridWidget_4);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        label_30 = new QLabel(gridWidget_4);
        label_30->setObjectName(QStringLiteral("label_30"));
        label_30->setFont(font1);

        gridLayout_4->addWidget(label_30, 5, 1, 1, 1);

        label_28 = new QLabel(gridWidget_4);
        label_28->setObjectName(QStringLiteral("label_28"));
        label_28->setFont(font2);

        gridLayout_4->addWidget(label_28, 1, 1, 1, 1);

        label_31 = new QLabel(gridWidget_4);
        label_31->setObjectName(QStringLiteral("label_31"));
        label_31->setFont(font1);

        gridLayout_4->addWidget(label_31, 4, 1, 1, 1);

        label_32 = new QLabel(gridWidget_4);
        label_32->setObjectName(QStringLiteral("label_32"));
        label_32->setFont(font1);
        label_32->setLayoutDirection(Qt::LeftToRight);

        gridLayout_4->addWidget(label_32, 3, 1, 1, 1);

        gridWidget_5 = new QWidget(Widget);
        gridWidget_5->setObjectName(QStringLiteral("gridWidget_5"));
        gridWidget_5->setGeometry(QRect(640, 100, 171, 171));
        gridLayout_5 = new QGridLayout(gridWidget_5);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        Iran_Score_Edit = new QLineEdit(gridWidget_5);
        Iran_Score_Edit->setObjectName(QStringLiteral("Iran_Score_Edit"));

        gridLayout_5->addWidget(Iran_Score_Edit, 2, 4, 1, 1);

        Katar_Score_Edit = new QLineEdit(gridWidget_5);
        Katar_Score_Edit->setObjectName(QStringLiteral("Katar_Score_Edit"));

        gridLayout_5->addWidget(Katar_Score_Edit, 3, 4, 1, 1);

        China_Score_Edit = new QLineEdit(gridWidget_5);
        China_Score_Edit->setObjectName(QStringLiteral("China_Score_Edit"));

        gridLayout_5->addWidget(China_Score_Edit, 1, 2, 1, 1);

        Uzbekistan_Score_Edit = new QLineEdit(gridWidget_5);
        Uzbekistan_Score_Edit->setObjectName(QStringLiteral("Uzbekistan_Score_Edit"));

        gridLayout_5->addWidget(Uzbekistan_Score_Edit, 1, 4, 1, 1);

        Syria_Score_Edit = new QLineEdit(gridWidget_5);
        Syria_Score_Edit->setObjectName(QStringLiteral("Syria_Score_Edit"));

        gridLayout_5->addWidget(Syria_Score_Edit, 3, 2, 1, 1);

        Korea_Score_Edit = new QLineEdit(gridWidget_5);
        Korea_Score_Edit->setObjectName(QStringLiteral("Korea_Score_Edit"));

        gridLayout_5->addWidget(Korea_Score_Edit, 2, 2, 1, 1);

        label_27 = new QLabel(gridWidget_5);
        label_27->setObjectName(QStringLiteral("label_27"));
        label_27->setFont(font1);

        gridLayout_5->addWidget(label_27, 1, 3, 1, 1);

        label_29 = new QLabel(gridWidget_5);
        label_29->setObjectName(QStringLiteral("label_29"));
        label_29->setFont(font1);

        gridLayout_5->addWidget(label_29, 2, 3, 1, 1);

        label_33 = new QLabel(gridWidget_5);
        label_33->setObjectName(QStringLiteral("label_33"));
        label_33->setFont(font1);

        gridLayout_5->addWidget(label_33, 3, 3, 1, 1);

        label_34 = new QLabel(Widget);
        label_34->setObjectName(QStringLiteral("label_34"));
        label_34->setGeometry(QRect(620, 70, 181, 41));
        label_34->setFont(font2);
        Nine_Analyze_text = new QTextBrowser(Widget);
        Nine_Analyze_text->setObjectName(QStringLiteral("Nine_Analyze_text"));
        Nine_Analyze_text->setGeometry(QRect(500, 330, 301, 191));
        label_10 = new QLabel(Widget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(530, 300, 241, 23));
        label_10->setFont(font2);
        After_nine_Button = new QPushButton(Widget);
        After_nine_Button->setObjectName(QStringLiteral("After_nine_Button"));
        After_nine_Button->setGeometry(QRect(620, 260, 111, 31));
        label_11 = new QLabel(Widget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(770, 530, 111, 21));
        label_11->setFont(font1);
        gridWidget_7 = new QWidget(Widget);
        gridWidget_7->setObjectName(QStringLiteral("gridWidget_7"));
        gridWidget_7->setGeometry(QRect(300, 330, 175, 201));
        gridLayout_7 = new QGridLayout(gridWidget_7);
        gridLayout_7->setSpacing(6);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        Syria_JingShengQiu_label = new QLabel(gridWidget_7);
        Syria_JingShengQiu_label->setObjectName(QStringLiteral("Syria_JingShengQiu_label"));
        Syria_JingShengQiu_label->setFont(font1);

        gridLayout_7->addWidget(Syria_JingShengQiu_label, 6, 1, 1, 1);

        label_40 = new QLabel(gridWidget_7);
        label_40->setObjectName(QStringLiteral("label_40"));
        label_40->setFont(font2);

        gridLayout_7->addWidget(label_40, 1, 1, 1, 1);

        Katar_JingShengQiu_label = new QLabel(gridWidget_7);
        Katar_JingShengQiu_label->setObjectName(QStringLiteral("Katar_JingShengQiu_label"));
        Katar_JingShengQiu_label->setFont(font1);

        gridLayout_7->addWidget(Katar_JingShengQiu_label, 7, 1, 1, 1);

        Uzbekistan_JingShengQiu_label = new QLabel(gridWidget_7);
        Uzbekistan_JingShengQiu_label->setObjectName(QStringLiteral("Uzbekistan_JingShengQiu_label"));
        Uzbekistan_JingShengQiu_label->setFont(font1);

        gridLayout_7->addWidget(Uzbekistan_JingShengQiu_label, 5, 1, 1, 1);

        Korea_JingShengQiu_label = new QLabel(gridWidget_7);
        Korea_JingShengQiu_label->setObjectName(QStringLiteral("Korea_JingShengQiu_label"));
        Korea_JingShengQiu_label->setFont(font1);

        gridLayout_7->addWidget(Korea_JingShengQiu_label, 4, 1, 1, 1);

        Iran_JingShengQiu_label = new QLabel(gridWidget_7);
        Iran_JingShengQiu_label->setObjectName(QStringLiteral("Iran_JingShengQiu_label"));
        Iran_JingShengQiu_label->setFont(font1);
        Iran_JingShengQiu_label->setLayoutDirection(Qt::LeftToRight);

        gridLayout_7->addWidget(Iran_JingShengQiu_label, 3, 1, 1, 1);

        China_JingShengQiu_label = new QLabel(gridWidget_7);
        China_JingShengQiu_label->setObjectName(QStringLiteral("China_JingShengQiu_label"));
        China_JingShengQiu_label->setFont(font1);

        gridLayout_7->addWidget(China_JingShengQiu_label, 8, 1, 1, 1);

        gridWidget_8 = new QWidget(Widget);
        gridWidget_8->setObjectName(QStringLiteral("gridWidget_8"));
        gridWidget_8->setGeometry(QRect(190, 330, 175, 201));
        gridLayout_8 = new QGridLayout(gridWidget_8);
        gridLayout_8->setSpacing(6);
        gridLayout_8->setContentsMargins(11, 11, 11, 11);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        Syria_JiFen_label = new QLabel(gridWidget_8);
        Syria_JiFen_label->setObjectName(QStringLiteral("Syria_JiFen_label"));
        Syria_JiFen_label->setFont(font1);

        gridLayout_8->addWidget(Syria_JiFen_label, 6, 1, 1, 1);

        Katar_JiFen_label = new QLabel(gridWidget_8);
        Katar_JiFen_label->setObjectName(QStringLiteral("Katar_JiFen_label"));
        Katar_JiFen_label->setFont(font1);

        gridLayout_8->addWidget(Katar_JiFen_label, 7, 1, 1, 1);

        Uzbekistan_JiFen_label = new QLabel(gridWidget_8);
        Uzbekistan_JiFen_label->setObjectName(QStringLiteral("Uzbekistan_JiFen_label"));
        Uzbekistan_JiFen_label->setFont(font1);

        gridLayout_8->addWidget(Uzbekistan_JiFen_label, 5, 1, 1, 1);

        Korea_JiFen_label = new QLabel(gridWidget_8);
        Korea_JiFen_label->setObjectName(QStringLiteral("Korea_JiFen_label"));
        Korea_JiFen_label->setFont(font1);

        gridLayout_8->addWidget(Korea_JiFen_label, 4, 1, 1, 1);

        Iran_JiFen_label = new QLabel(gridWidget_8);
        Iran_JiFen_label->setObjectName(QStringLiteral("Iran_JiFen_label"));
        Iran_JiFen_label->setFont(font1);
        Iran_JiFen_label->setLayoutDirection(Qt::LeftToRight);

        gridLayout_8->addWidget(Iran_JiFen_label, 3, 1, 1, 1);

        China_JiFen_label = new QLabel(gridWidget_8);
        China_JiFen_label->setObjectName(QStringLiteral("China_JiFen_label"));
        China_JiFen_label->setFont(font1);

        gridLayout_8->addWidget(China_JiFen_label, 8, 1, 1, 1);

        label_47 = new QLabel(gridWidget_8);
        label_47->setObjectName(QStringLiteral("label_47"));
        label_47->setFont(font2);

        gridLayout_8->addWidget(label_47, 0, 1, 1, 1);

        gridWidget_9 = new QWidget(Widget);
        gridWidget_9->setObjectName(QStringLiteral("gridWidget_9"));
        gridWidget_9->setGeometry(QRect(50, 330, 175, 201));
        gridLayout_9 = new QGridLayout(gridWidget_9);
        gridLayout_9->setSpacing(6);
        gridLayout_9->setContentsMargins(11, 11, 11, 11);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        label_9 = new QLabel(gridWidget_9);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setFont(font1);
        label_9->setLayoutDirection(Qt::LeftToRight);

        gridLayout_9->addWidget(label_9, 3, 1, 1, 1);

        label_12 = new QLabel(gridWidget_9);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setFont(font1);

        gridLayout_9->addWidget(label_12, 6, 1, 1, 1);

        label_53 = new QLabel(gridWidget_9);
        label_53->setObjectName(QStringLiteral("label_53"));
        label_53->setFont(font1);

        gridLayout_9->addWidget(label_53, 7, 1, 1, 1);

        label_54 = new QLabel(gridWidget_9);
        label_54->setObjectName(QStringLiteral("label_54"));
        label_54->setFont(font1);

        gridLayout_9->addWidget(label_54, 5, 1, 1, 1);

        label_55 = new QLabel(gridWidget_9);
        label_55->setObjectName(QStringLiteral("label_55"));
        label_55->setFont(font1);

        gridLayout_9->addWidget(label_55, 8, 1, 1, 1);

        label_56 = new QLabel(gridWidget_9);
        label_56->setObjectName(QStringLiteral("label_56"));
        label_56->setFont(font1);

        gridLayout_9->addWidget(label_56, 4, 1, 1, 1);

        label_57 = new QLabel(gridWidget_9);
        label_57->setObjectName(QStringLiteral("label_57"));
        label_57->setFont(font2);

        gridLayout_9->addWidget(label_57, 1, 1, 1, 1);

        label_35 = new QLabel(Widget);
        label_35->setObjectName(QStringLiteral("label_35"));
        label_35->setGeometry(QRect(120, 310, 311, 23));
        label_35->setFont(font2);
        Now_Button = new QPushButton(Widget);
        Now_Button->setObjectName(QStringLiteral("Now_Button"));
        Now_Button->setGeometry(QRect(180, 270, 161, 31));
        graphicsView = new QGraphicsView(Widget);
        graphicsView->setObjectName(QStringLiteral("graphicsView"));
        graphicsView->setGeometry(QRect(810, 230, 161, 131));

        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QApplication::translate("Widget", "Widget", 0));
        label->setText(QApplication::translate("Widget", "\344\270\226\351\242\204\350\265\233\345\233\275\350\266\263\345\260\217\347\273\204\350\265\233\345\207\272\347\272\277\345\210\206\346\236\220", 0));
        label_3->setText(QApplication::translate("Widget", "       \344\274\212   \346\234\227", 0));
        label_6->setText(QApplication::translate("Widget", "       \345\217\231\345\210\251\344\272\232", 0));
        label_7->setText(QApplication::translate("Widget", "       \345\215\241\345\241\224\345\260\224", 0));
        label_5->setText(QApplication::translate("Widget", "     \344\271\214\345\205\271\345\210\253\345\205\213\346\226\257\345\235\246", 0));
        label_8->setText(QApplication::translate("Widget", "       \344\270\255  \345\233\275", 0));
        label_4->setText(QApplication::translate("Widget", "       \351\237\251   \345\233\275", 0));
        label_2->setText(QApplication::translate("Widget", "   A\347\273\204\347\220\203\351\230\237\345\210\227\350\241\250", 0));
        label_17->setText(QApplication::translate("Widget", "         9", 0));
        label_13->setText(QApplication::translate("Widget", "    \345\275\223\345\211\215\347\247\257\345\210\206", 0));
        label_18->setText(QApplication::translate("Widget", "         7", 0));
        label_16->setText(QApplication::translate("Widget", "         12", 0));
        label_14->setText(QApplication::translate("Widget", "         13", 0));
        label_15->setText(QApplication::translate("Widget", "         20", 0));
        label_19->setText(QApplication::translate("Widget", "         6", 0));
        label_20->setText(QApplication::translate("Widget", "        -1", 0));
        label_21->setText(QApplication::translate("Widget", "   \345\275\223\345\211\215\345\207\200\350\203\234\347\220\203", 0));
        label_22->setText(QApplication::translate("Widget", "        -4", 0));
        label_23->setText(QApplication::translate("Widget", "         0", 0));
        label_24->setText(QApplication::translate("Widget", "         1", 0));
        label_25->setText(QApplication::translate("Widget", "         8", 0));
        label_26->setText(QApplication::translate("Widget", "        -4", 0));
        label_30->setText(QApplication::translate("Widget", "   \345\217\231\345\210\251\344\272\232---\345\215\241\345\241\224\345\260\224", 0));
        label_28->setText(QApplication::translate("Widget", "   \347\254\254\344\271\235\350\275\256\346\257\224\350\265\233", 0));
        label_31->setText(QApplication::translate("Widget", "    \351\237\251\345\233\275---\344\274\212\346\234\227", 0));
        label_32->setText(QApplication::translate("Widget", "\344\270\255\345\233\275---\344\271\214\345\205\271\345\210\253\345\205\213\346\226\257\345\235\246", 0));
        China_Score_Edit->setText(QString());
        Uzbekistan_Score_Edit->setText(QString());
        label_27->setText(QApplication::translate("Widget", "\357\274\232", 0));
        label_29->setText(QApplication::translate("Widget", "\357\274\232", 0));
        label_33->setText(QApplication::translate("Widget", "\357\274\232", 0));
        label_34->setText(QApplication::translate("Widget", "   \347\254\254\344\271\235\350\275\256\346\257\224\350\265\233\347\273\223\346\236\234", 0));
        label_10->setText(QApplication::translate("Widget", "\347\254\254\344\271\235\350\275\256\346\257\224\350\265\233\345\220\216\345\207\272\347\272\277\345\210\206\346\236\220\347\273\223\346\236\234", 0));
        After_nine_Button->setText(QApplication::translate("Widget", "\347\254\254\344\271\235\350\275\256\350\265\233\345\220\216\345\210\206\346\236\220", 0));
        label_11->setText(QApplication::translate("Widget", "lizhen@sia.cn", 0));
        Syria_JingShengQiu_label->setText(QString());
        label_40->setText(QApplication::translate("Widget", "    \345\207\200\350\203\234\347\220\203", 0));
        Katar_JingShengQiu_label->setText(QString());
        Uzbekistan_JingShengQiu_label->setText(QString());
        Korea_JingShengQiu_label->setText(QString());
        Iran_JingShengQiu_label->setText(QString());
        China_JingShengQiu_label->setText(QString());
        Syria_JiFen_label->setText(QString());
        Katar_JiFen_label->setText(QString());
        Uzbekistan_JiFen_label->setText(QString());
        Korea_JiFen_label->setText(QString());
        Iran_JiFen_label->setText(QString());
        China_JiFen_label->setText(QString());
        label_47->setText(QApplication::translate("Widget", "     \347\247\257\345\210\206", 0));
        label_9->setText(QApplication::translate("Widget", "       \344\274\212   \346\234\227", 0));
        label_12->setText(QApplication::translate("Widget", "       \345\217\231\345\210\251\344\272\232", 0));
        label_53->setText(QApplication::translate("Widget", "       \345\215\241\345\241\224\345\260\224", 0));
        label_54->setText(QApplication::translate("Widget", "     \344\271\214\345\205\271\345\210\253\345\205\213\346\226\257\345\235\246", 0));
        label_55->setText(QApplication::translate("Widget", "       \344\270\255  \345\233\275", 0));
        label_56->setText(QApplication::translate("Widget", "       \351\237\251   \345\233\275", 0));
        label_57->setText(QApplication::translate("Widget", "   A\347\273\204\347\220\203\351\230\237\345\210\227\350\241\250", 0));
        label_35->setText(QApplication::translate("Widget", "\347\254\254\344\271\235\350\275\256\346\257\224\350\265\233\345\220\216\347\247\257\345\210\206\345\222\214\345\207\200\350\203\234\347\220\203\346\203\205\345\206\265", 0));
        Now_Button->setText(QApplication::translate("Widget", "\347\254\254\344\271\235\350\275\256\350\265\233\345\220\216\347\247\257\345\210\206\345\207\200\350\203\234\347\220\203\350\256\241\347\256\227", 0));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
