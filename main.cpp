/**
 * 李震
 * 我的码云：https://git.oschina.net/git-lizhen
 * 我的CSDN：http://blog.csdn.net/weixin_38215395
 * 联系：lizhen@sia.cn
 */
#include "widget.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    w.show();
    return a.exec();
}
