#-------------------------------------------------
#
# Project created by QtCreator 2017-08-30T09:27:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GuoZu
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    team.cpp

HEADERS  += widget.h \
    team.h

FORMS    += widget.ui
