/**
 * 李震
 * 我的码云：https://git.oschina.net/git-lizhen
 * 我的CSDN：http://blog.csdn.net/weixin_38215395
 * 联系：lizhen@sia.cn
 */
#ifndef TEAM_H
#define TEAM_H

class Team
{
public:
    //构造函数,球队当前积分、当前净胜球数，是否为主场标志,参与上一场比赛结果
    Team(int JiFen_Present,int JingShengQiu_Present,bool ZhuChang,int ZhuChangJinQiuShu,int KeChangJinQiuShu);
    ~Team();
    int JiFen_After();                //获取比赛完后的球队积分
    int JingShengQiu_After();         //获取比赛完后的球队净胜球数

private:
    int JiFenAfter;                   //比赛完后的球队积分
    int JingShengQiuAfter;            //比赛完后的球队净胜球数

};

#endif // TEAM_H
