/**
 * 李震
 * 我的码云：https://git.oschina.net/git-lizhen
 * 我的CSDN：http://blog.csdn.net/weixin_38215395
 * 联系：lizhen@sia.cn
 */
#include "widget.h"
#include "ui_widget.h"
#include "team.h"
#include "QtMath"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    //显示“奏国歌”图片
    this->image = new QImage();
    image->load("F:/Self_Learning/Qt/GuoZu/1.jpg");
    QGraphicsScene *scene = new QGraphicsScene;
    scene->addPixmap(QPixmap::fromImage(*image));
    ui->graphicsView->setScene(scene);
    ui->graphicsView->resize(ui->graphicsView->width(),ui->graphicsView->height());
    ui->graphicsView->show();

    //连接槽
    //将“第九轮赛后积分净胜球计算”按钮与Widget类的公共成员函数“Calculate_After_Nine”连接
    connect(ui->Now_Button,&QPushButton::clicked,this,&Widget::Calculate_After_Nine);
    //将“第九轮赛后分析”按钮与Widget类的公共成员函数“Analyze”连接
    connect(ui->After_nine_Button,&QPushButton::clicked,this,&Widget::Analyze);
}

Widget::~Widget()
{
    delete ui;
}

//对第九轮每场比赛进行分析,获得赛后的积分和净胜球数
void Widget::Calculate_After_Nine()
{
    //非法输入验证
    if(!ui->China_Score_Edit->text().isEmpty() && !ui->Uzbekistan_Score_Edit->text().isEmpty())
    {
        //中国---乌兹别克斯坦(1-0)
        //实例化Team类的对象China_Nine,参数依次为：当前积分，当前净胜球数，是否为主场（bool值),比赛结果主场得分，比赛结果客场得分
        Team China_Nine(6,-4,1,ui->China_Score_Edit->text().toInt(),ui->Uzbekistan_Score_Edit->text().toInt());
        China_Nine_JiFen=China_Nine.JiFen_After();   //获取中国队第九轮赛后积分
        China_Nine_JingShengQiu=China_Nine.JingShengQiu_After();//获取中国队第九轮赛后净胜球数
        //实例化Team类的对象Uzbekistan_Nine,参数依次为：当前积分，当前净胜球数，是否为主场（bool值),比赛结果主场得分，比赛结果客场得分
        Team Uzbekistan_Nine(12,0,0,ui->China_Score_Edit->text().toInt(),ui->Uzbekistan_Score_Edit->text().toInt());
        Uzbekistan_Nine_JiFen=Uzbekistan_Nine.JiFen_After();  //获取乌兹别克斯坦队第九轮赛后积分
        Uzbekistan_Nine_JingShengQiu=Uzbekistan_Nine.JingShengQiu_After();//获取乌兹别克斯坦队第九轮赛后净胜球数
        //显示
        ui->China_JiFen_label->setAlignment(Qt::AlignHCenter);  //居中显示
        ui->China_JiFen_label->setText(QString::number(China_Nine_JiFen));//将数字类型转化为QString类型，并显示
        ui->China_JingShengQiu_label->setAlignment(Qt::AlignHCenter);//居中显示
        ui->China_JingShengQiu_label->setText(QString::number(China_Nine_JingShengQiu));//将数字类型转化为QString类型，并显示

        ui->Uzbekistan_JiFen_label->setAlignment(Qt::AlignHCenter);
        ui->Uzbekistan_JiFen_label->setText(QString::number(Uzbekistan_Nine_JiFen));
        ui->Uzbekistan_JingShengQiu_label->setAlignment(Qt::AlignHCenter);
        ui->Uzbekistan_JingShengQiu_label->setText(QString::number(Uzbekistan_Nine_JingShengQiu));
    }
    else
        //新建提示对话框，用于提示未输入第九轮比分
        QMessageBox::warning(NULL, "Warning!", "请输入“中国---乌兹别克斯坦”比赛结果！  ",QMessageBox::Yes);

    //非法输入验证
    if(!ui->Syria_Score_Edit->text().isEmpty() && !ui->Katar_Score_Edit->text().isEmpty())
    {
        //叙利亚---卡塔尔(3-1)
        //实例化Team类的对象Syria_Nine,参数依次为：当前积分，当前净胜球数，是否为主场（bool值),比赛结果主场得分，比赛结果客场得分
        Team Syria_Nine(9,-1,1,ui->Syria_Score_Edit->text().toInt(),ui->Katar_Score_Edit->text().toInt());
        Syria_Nine_JiFen=Syria_Nine.JiFen_After();
        Syria_Nine_JingShengQiu=Syria_Nine.JingShengQiu_After();
        //实例化Team类的对象Katar_Nine,参数依次为：当前积分，当前净胜球数，是否为主场（bool值),比赛结果主场得分，比赛结果客场得分
        Team Katar_Nine(7,-4,0,ui->Syria_Score_Edit->text().toInt(),ui->Katar_Score_Edit->text().toInt());
        Katar_Nine_JiFen=Katar_Nine.JiFen_After();
        Katar_Nine_JingShengQiu=Katar_Nine.JingShengQiu_After();

        //显示
        ui->Syria_JiFen_label->setAlignment(Qt::AlignHCenter);
        ui->Syria_JiFen_label->setText(QString::number(Syria_Nine_JiFen));
        ui->Syria_JingShengQiu_label->setAlignment(Qt::AlignHCenter);
        ui->Syria_JingShengQiu_label->setText(QString::number(Syria_Nine_JingShengQiu));

        ui->Katar_JiFen_label->setAlignment(Qt::AlignHCenter);
        ui->Katar_JiFen_label->setText(QString::number(Katar_Nine_JiFen));
        ui->Katar_JingShengQiu_label->setAlignment(Qt::AlignHCenter);
        ui->Katar_JingShengQiu_label->setText(QString::number(Katar_Nine_JingShengQiu));
    }
    else
        //新建提示对话框，用于提示未输入第九轮比分
        QMessageBox::warning(NULL, "Warning!", "请输入“叙利亚---卡塔尔”比赛结果！  ",QMessageBox::Yes);

    //非法输入验证
    if(!ui->Iran_Score_Edit->text().isEmpty() && !ui->Korea_Score_Edit->text().isEmpty())
    {
        //伊朗---韩国(0-0)
        //实例化Team类的对象Korea_Nine,参数依次为：当前积分，当前净胜球数，是否为主场（bool值),比赛结果主场得分，比赛结果客场得分
        Team Korea_Nine(13,1,1,ui->Iran_Score_Edit->text().toInt(),ui->Korea_Score_Edit->text().toInt());
        Korea_Nine_JiFen=Korea_Nine.JiFen_After();
        Korea_Nine_JingShengQiu=Korea_Nine.JingShengQiu_After();
        //实例化Team类的对象Iran_Nine,参数依次为：当前积分，当前净胜球数，是否为主场（bool值),比赛结果主场得分，比赛结果客场得分
        Team Iran_Nine(20,8,0,ui->Iran_Score_Edit->text().toInt(),ui->Korea_Score_Edit->text().toInt());
        Iran_Nine_JiFen=Iran_Nine.JiFen_After();
        Iran_Nine_JingShengQiu=Iran_Nine.JingShengQiu_After();

        //显示
        ui->Iran_JiFen_label->setAlignment(Qt::AlignHCenter);
        ui->Iran_JiFen_label->setText(QString::number(Iran_Nine_JiFen));
        ui->Iran_JingShengQiu_label->setAlignment(Qt::AlignHCenter);
        ui->Iran_JingShengQiu_label->setText(QString::number(Iran_Nine_JingShengQiu));

        ui->Korea_JiFen_label->setAlignment(Qt::AlignHCenter);
        ui->Korea_JiFen_label->setText(QString::number(Korea_Nine_JiFen));
        ui->Korea_JingShengQiu_label->setAlignment(Qt::AlignHCenter);
        ui->Korea_JingShengQiu_label->setText(QString::number(Korea_Nine_JingShengQiu));
    }
    else
        //新建提示对话框，用于提示未输入第九轮比分
        QMessageBox::warning(NULL, "Warning!", "请输入“伊朗---韩国”比赛结果！  ",QMessageBox::Yes);
}
//分析国足出线时，球队间可能的比分
void Widget::Analyze()
{
    //刷新显示内容
    ui->Nine_Analyze_text->setPlainText("");

    //变量声明
    int Syria_Ten_JiFen[36];
    int Syria_Ten_JingShengQiu[36];
    int Katar_Ten_JiFen[36];
    int Katar_Ten_JingShengQiu[36];

    int Uzbekistan_Ten_JiFen[36];
    int Uzbekistan_Ten_JingShengQiu[36];
    int China_Ten_JiFen[36];
    int China_Ten_JingShengQiu[36];
    //假定每场比赛每队最多进球数为5个，因为国足的对手不是菲律宾
    int ZhuChangJinQiu=0,KeChangJinQiu=0,maxJinQiuShu=5;
    //比赛模拟
    int i=0;             //索引值
    for(ZhuChangJinQiu=0;ZhuChangJinQiu<=maxJinQiuShu;ZhuChangJinQiu++)
        for(KeChangJinQiu=0;KeChangJinQiu<=maxJinQiuShu;KeChangJinQiu++)
        {
            //卡塔尔---中国
            Team Katar_Ten(Katar_Nine_JiFen,Katar_Nine_JingShengQiu,1,ZhuChangJinQiu,KeChangJinQiu);
            Katar_Ten_JiFen[i]=Katar_Ten.JiFen_After();
            Katar_Ten_JingShengQiu[i]=Katar_Ten.JingShengQiu_After();

            Team China_Ten(China_Nine_JiFen,China_Nine_JingShengQiu,0,ZhuChangJinQiu,KeChangJinQiu);
            China_Ten_JiFen[i]=China_Ten.JiFen_After();
            China_Ten_JingShengQiu[i]=China_Ten.JingShengQiu_After();

            //伊朗---叙利亚
            Team Syria_Ten(Syria_Nine_JiFen,Syria_Nine_JingShengQiu,0,ZhuChangJinQiu,KeChangJinQiu);
            Syria_Ten_JiFen[i]=Syria_Ten.JiFen_After();
            Syria_Ten_JingShengQiu[i]=Syria_Ten.JingShengQiu_After();
            //乌兹别克斯坦---韩国
            Team Uzbekistan_Ten(Uzbekistan_Nine_JiFen,Uzbekistan_Nine_JingShengQiu,1,ZhuChangJinQiu,KeChangJinQiu);
            Uzbekistan_Ten_JiFen[i]=Uzbekistan_Ten.JiFen_After();
            Uzbekistan_Ten_JingShengQiu[i]=Uzbekistan_Ten.JingShengQiu_After();

            //国足出线条件
            if(China_Ten_JiFen[i]==12)     //首先要赢球，拿到3分
            {
                //对每个符合i值之前的每场模拟比赛进行判断，这样不会存在遗漏
                for(int k=0;k<=i;k++)     //乌兹别克斯坦---韩国模拟比赛索引值
                    for(int m=0;m<=i;m++) //伊朗---叙利亚模拟比赛索引值
                {
                    //国足出线条件：在if中取“非”了。若第k次模拟比赛乌兹别克斯坦队的积分为12分并且其净胜球小于中国队，则输出该次比赛的结果
                    //这里利用“int(k/6)%6”和“int(k%6)”分别代表主客场进球，因为是6个一循环
                    //同理，对叙利亚的第m次模拟比赛做同样的处理
                    if(((Uzbekistan_Ten_JiFen[k]==12 && Uzbekistan_Ten_JingShengQiu[k]<China_Ten_JingShengQiu[i])
                       && (Syria_Ten_JiFen[m]==12 && Syria_Ten_JingShengQiu[m]<China_Ten_JingShengQiu[i])))
                    {
                        continue;
                    }
                    else
                    {
                        QString str1=+"出线要求的比分：";
                        ui->Nine_Analyze_text->append(str1);
                        QString str="卡塔尔---中国"+QString::number(int(i/6)%6)+":"
                            +QString::number(int(i%6))+"    "+"乌兹别克斯坦---韩国"+QString::number(int(k/6)%6)+":"
                                +QString::number(int(k%6))+"    "+"叙利亚---伊朗"+QString::number(int(m/6)%6)+":"
                                +QString::number(int(m%6));
                        //行追加显示，本次显示完，下一次接着在下一行显示
                        ui->Nine_Analyze_text->append(str);
                    }
                }
            }
          i++;   //国足索引值加1
        }
}










