/**
 * 李震
 * 我的码云：https://git.oschina.net/git-lizhen
 * 我的CSDN：http://blog.csdn.net/weixin_38215395
 * 联系：lizhen@sia.cn
 */
#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTextEdit>
#include <QMessageBox>
#include <QImage>
#include <QGraphicsView>
#include <QGraphicsScene>

using namespace std;

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;
    int DeFen;

    //变量声明,球队积分和净胜球
    int Iran_Nine_JiFen;
    int Iran_Nine_JingShengQiu;

    int Korea_Nine_JiFen;
    int Korea_Nine_JingShengQiu;

    int Syria_Nine_JiFen;
    int Syria_Nine_JingShengQiu;

    int Katar_Nine_JiFen;
    int Katar_Nine_JingShengQiu;

    int Uzbekistan_Nine_JiFen;
    int Uzbekistan_Nine_JingShengQiu;

    int China_Nine_JiFen;
    int China_Nine_JingShengQiu;

    QImage *image;

private slots:
    void Calculate_After_Nine();    //第九轮过后积分和净胜就计算
    void Analyze();                //第九轮过后出线情况分析

};

#endif // WIDGET_H
